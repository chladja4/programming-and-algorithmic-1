#include <stdio.h>
#include <stdlib.h>
typedef struct BST {
  int data;
  BST * left;
  BST * right;
}BST;

BST * GetNewNode(int DATA){
  BST * NewNode=(BST *)malloc(sizeof(BST));
  NewNode->left=NULL;
  NewNode->right=NULL;
  NewNode->data=DATA;
  return NewNode;
}

BST * Insert(BST * root,int data){
  if(root==NULL){
    root= GetNewNode(data);
    return root;
  }
  else if(data<root->data){
    root->left= Insert(root->left,data);
  }
  else
    root->right= Insert(root->right,data);
  return root;
}

int Search(BST * root,int DATA){
if(root==NULL) return 0;
else if(root->data==DATA) return 1;
else if(DATA < root->data) return Search(root->left,DATA);
else return Search(root->right,DATA);
}
int MAX(int a, int b){
  if(a>b) return a;
  return b;
}

int height(BST * root){
  if(root==NULL) return -1;
  return MAX(height(root->left),height(root->right))+1;
}

void PrintInOrder(BST * root){
    if(root==NULL) return;
    PrintInOrder(root->left);
    printf("%d ",root->data);
    PrintInOrder(root->right);
}

void PrintReverseOrder(BST * root){
  if(root==NULL) return;
  printf("%d ",root->data);
  PrintInOrder(root->right);
  PrintInOrder(root->left);
}

int main (){
  BST * root=NULL;
  root=Insert(root,5);
  root=Insert(root,15);
  root=Insert(root,13);
  root=Insert(root,6);
  root=Insert(root,7);
  PrintInOrder(root);
  printf("\n");
  PrintReverseOrder(root);
  printf("\n");
  printf("%d \n",height(root));

}