#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
//[1:2, 3:4, 7:10, 2:4, 5:20, 17:25, 8:3] test tolls
typedef struct TOLLS{
  int Distance;
  int Price;
}TOLLS;

int Compare(const void *a, const void * b){
  return *(int *)a - *(int *)b;}

void PrintArr(int count,TOLLS * TollsArray){
  for (int i=0;i<count;i++) printf("%d:%d ",TollsArray[i].Distance,TollsArray[i].Price);}

void FindCombination(int * MAX, TOLLS * TollsArray,int count,int Discount) {
  int Total;  // promenna pro tvorbu vsech souctu
  for (int i = 0; i < count; i++) {
    Total = TollsArray[i].Price; // Do souctu ulozim itej prvek od ktereho zacinam a ve druhem forcyklu k nemu pricitam
    for (int j = i + 1; j < count; j++) {
      if (Total + TollsArray[j].Price <= Discount && Total + TollsArray[j].Price > *MAX) *MAX = Total + TollsArray[j].Price;
      if(*MAX==Discount)return; //nasli jsme max moznou slevu, prestavame hledat
      Total += TollsArray[j].Price; // forcyklus co hazi soucty 2,3,4,5... mytnejch bran
    }
  }
}

void FindSingleToll(int * MAX,TOLLS * TollsArray,int count, int Discount){ //MAX jako vystupni parametr
  for(int i=0;i<count;i++){
    if(TollsArray[i].Price <= Discount)
      if (TollsArray[i].Price > *MAX) *MAX = TollsArray[i].Price; //Projede jednotlive brany a vybere tu nejdrazsi, ale zaroven <= sleva
    if(*MAX==Discount) return; // nasli jsem max moznou slevu, prestavame hledat
  }
}

int main (){
  int Price,Distance,Total=0,count=0,Discount,MAX,res;
  TOLLS * TollsArray=(TOLLS*)malloc(sizeof(TOLLS)*1000);
  printf("Zadej Brany:\n");
  scanf("[");
while(scanf("%d:%d,",&Distance,&Price)==2){
    TollsArray[count].Distance=Distance;
    TollsArray[count].Price=Price;
    Total+=Price;
    count++;
}

qsort(TollsArray,count,sizeof(TOLLS),Compare); //sort array to get Tolls to the right order
scanf(" ]");
printf("Slevy:\n");

while(1){
  MAX=INT_MIN;
  if((res=scanf("%d",&Discount))==EOF) break;
  if(res!=1) break;
  FindSingleToll(&MAX,TollsArray,count,Discount);
    if(MAX!=Discount)FindCombination(&MAX,TollsArray,count,Discount); //pokud jsem nenasel max moznou slevu mezi jednotlivymi myty hledam soucty
  if(MAX==INT_MIN) printf ("Slevu nelze uplatnit.\n");
  else printf("%d (%d/%d)\n",Total-MAX,MAX,Discount); //Vystup Cena Po Sleve (Sleva/Vyuzito)
}
return 0;
}
//Chláďa 12/02/2022