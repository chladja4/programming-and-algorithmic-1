#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#endif /* __PROGTEST__ */
#define DEN 292886


int mesicedny[12]={31,28,31,30,31,30,31,31,30,31,30,31};
int leapyear(int year){
    if(year%4000==0) return 0;
    else if(year%400==0) return 1;
    else if(year%100==0) return 0;
    else if(year%4==0) return 1;
    else return 0;
    /*if(year%4!=0) return 0;
    if(year%100!=0) return 1;
    if(year%400!=0) return 0;
    if(year%4000!=0) return 1;*/
    return 1;
}

int maxdaynum(int den, int mesic, int rok){
    int mesicedny[12]={31,28,31,30,31,30,31,31,30,31,30,31};
    if(mesic==2){
        if(den > leapyear(rok) + 28) return 0;
        return 1;
    }
    if(den>mesicedny[mesic-1]) return 0;
    return 1;
}

int input(int y1 ,int y2, int m1, int m2, int d1, int d2,int h1, int h2, int i1, int i2){
    if(y1<1600||y2<1600||y1>y2) return 0;
    if(h1>23||h2>23||h1<0||h2<0||i1>59||i2>59||i1<0||i2<0||m1>12||m2>12||m2<1||m1<1||d1<1||d2<1) return 0;
    if(!maxdaynum(d1,m1,y1) || !maxdaynum(d2,m2,y2)) return 0;
    return 1;
}

long long yeardiff(int rok1, int rok2 ){
    long long pocetdnu=0;
    for (; rok1 <= rok2; rok1++){
        if(leapyear(rok1)) pocetdnu+=366;
        else pocetdnu+=365;
    }
    return pocetdnu;
}

/*long long odzacatkuroku(int den, int mesic,int rok){
    if(leapyear(rok) && mesic > 2) den++;
    int sum=0;
    for(int i=0;i<mesic;i++) sum+=mesicedny[i];
    sum+=den;
    return (sum-1);
}*/

long long odzacatkuroku(int den, int mesic,int rok){
    if(leapyear(rok) && mesic > 2) den++;
    int sum=0;
    for(int i=1;i<mesic;i++) sum+=mesicedny[i-1];
    sum+=den;
    return sum;

}

long long dokonceroku(int den, int mesic,int rok) {
    if (leapyear(rok) && mesic < 3) den++;
    int sum = 0;
    for (; mesic < 12; mesic++) {
        sum += mesicedny[mesic - 1];
    }
    sum = sum + mesicedny[mesic - 1] - den + 1;
    return sum;
}

long long dokoncedne (int h,int m){
    if(h==0&&m==0) return 0;
    int pole1[11]={30,26,21,19,16,13,12,7,3,2};
    int pole2[7]={20,16,11,9,6,3};
    int pole3[24]={86,82,77,75,72,69,68,63,59,58,52,48,43,41,38,35,34,29,25,24,17,13,8,6};
    return pole3[h]+pole1[m%10]+pole2[m/10]+(( 5 - (m/10) )*30)+(12200*(23-h))+((60-m)*200);
    //if(h==0&&m==0) return DEN;
}
long long odzacatkudne (int h, int m){
    if(h==0&&m==0){
        return DEN;
    }
    return DEN-dokoncedne(h,m);
}

long long energyConsumption ( int y1, int m1, int d1, int h1, int i1,
                              int y2, int m2, int d2, int h2, int i2, long long int * consumption ){
    if(!input(y1 ,y2, m1, m2, d1, d2,h1, h2, i1, i2)) return 0;
    if(!maxdaynum(d1,m1,y1)) return 0;
    if(!maxdaynum(d2,m2,y2)) return 0;
    if(odzacatkudne(h2,i2)<0 ) return 0;
    /*fixed
    if(y1==y2&&m1==m2&&d1==d2) {
        *consumption = odzacatkudne(h2, i2) - odzacatkudne(h1, i1);
        if (*consumption < 0) return 0;
        return 1;
    }
    */
    if(y1==y2&&m1==m2&&d1==d2){
        *consumption= dokoncedne(h1,i1)- dokoncedne(h2,i2);
        if(dokoncedne(h1,i1)==0) *consumption=*consumption*(-1);
        if (*consumption < 0) return 0;
        return 1;
    }
    else if(y1==y2&&m1==m2){
        *consumption=dokoncedne(h1,i1)+ odzacatkudne(h2,i2)+((d2-d1-1)*DEN);
        if (*consumption < 0) return 0;
        return 1;
    }
    else if(y1==y2){
        *consumption=dokoncedne(h1,i1)+ odzacatkudne(h2,i2)+ ( DEN+(odzacatkuroku(d2-1,m2,y2)*DEN)-(odzacatkuroku(d1+1,m1,y1)*DEN) );
        if (*consumption < 0) return 0;
        //if(d1==1&& !leapyear(y1)) {*consumption=*consumption+DEN; return 1;}

        return 1;
    }
    else{
        *consumption=dokoncedne(h1,i1)+odzacatkudne(h2,i2)+ yeardiff(y1+1,y2-1)*DEN+ odzacatkuroku(d2-1,m2,y2)*DEN + (dokonceroku(d1+1,m1,y1))*DEN;


        if (*consumption < 0) return 0;
        return 1;}
}






#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
  long long int consumption;

  assert ( energyConsumption ( 2021, 10,  1, 13, 15,
                               2021, 10,  1, 18, 45, &consumption ) == 1
           && consumption == 67116LL );
  assert ( energyConsumption ( 2021, 10,  1, 13, 15,
                               2021, 10,  2, 11, 20, &consumption ) == 1
           && consumption == 269497LL );
  assert ( energyConsumption ( 2021,  1,  1, 13, 15,
                               2021, 10,  5, 11, 20, &consumption ) == 1
           && consumption == 81106033LL );
  assert ( energyConsumption ( 2024,  1,  1, 13, 15,
                               2024, 10,  5, 11, 20, &consumption ) == 1
           && consumption == 81398919LL );
  assert ( energyConsumption ( 1900,  1,  1, 13, 15,
                               1900, 10,  5, 11, 20, &consumption ) == 1
           && consumption == 81106033LL );
  assert ( energyConsumption ( 2021, 10,  1,  0,  0,
                               2021, 10,  1, 12,  0, &consumption ) == 1
           && consumption == 146443LL );
  assert ( energyConsumption ( 2021, 10,  1,  0, 15,
                               2021, 10,  1,  0, 25, &consumption ) == 1
           && consumption == 2035LL );
  assert ( energyConsumption ( 2021, 10,  1, 12,  0,
                               2021, 10,  1, 12,  0, &consumption ) == 1
           && consumption == 0LL );
  assert ( energyConsumption ( 2021, 10,  1, 12,  0,
                               2021, 10,  1, 12,  1, &consumption ) == 1
           && consumption == 204LL );
  assert ( energyConsumption ( 2021, 11,  1, 12,  0,
                               2021, 10,  1, 12,  0, &consumption ) == 0 );
  assert ( energyConsumption ( 2021, 10, 32, 12,  0,
                               2021, 11, 10, 12,  0, &consumption ) == 0 );
  assert ( energyConsumption ( 2100,  2, 29, 12,  0,
                               2100,  2, 29, 12,  0, &consumption ) == 0 );
  assert ( energyConsumption ( 2400,  2, 29, 12,  0,
                               2400,  2, 29, 12,  0, &consumption ) == 1
           && consumption == 0LL );
  return 0;
}
#endif /* __PROGTEST__ */
