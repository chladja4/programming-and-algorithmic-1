#ifndef __PROGTEST__
#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#endif /* __PROGTEST__ */
struct words {
  int zacatek;
  int konec;
};
typedef struct words words;
words *ptr;
words *ptr2;

int indexarr(const char *a, words *ptr) {
  int read = 0, position = 0;
  for (int i = 0; i < (int)strlen(a); i++) {
    if (isalnum(a[i]) && !read) {
      ptr[position].zacatek = i;
      read = 1;
    } else if (read && (!isalnum(a[i]))) {
      ptr[position].konec = i - 1;
      read = 0;
      position++;
    }
  }
  if (read == 1) {
    ptr[position].konec = strlen(a) - 1;
    position++;
  }
    return position;
}

int compareeachletter(const char* word1, const char* word2, int j, words pttr2){
  for (int i = ptr[j].zacatek; i < ptr[j].konec + 1; i++){
    if (tolower(word1[i]) != tolower(word2[i - ptr[j].zacatek + pttr2.zacatek])) return 0;
  }
  return 1;
}

int compare(words pttr2,int wordcount, const char *sentence1, const char *sentence2){
  int delka = 1+pttr2.konec - pttr2.zacatek;
  for (int i = 0; i < wordcount; i++){
    if(1+ptr[i].konec - ptr[i].zacatek != delka) continue;
    if(compareeachletter(sentence1, sentence2, i, pttr2)) return 1;
  }
  return 0;
}
int sameWords(const char *a, const char *b) {
  ptr = (words *)malloc(strlen(a) * sizeof(words));
  ptr2 = (words *)malloc(strlen(b) * sizeof(words));
  int wordcount = indexarr(a, ptr);
  int wordcount2 = indexarr(b, ptr2);
  for (int i = 0; i < wordcount2; i++) if (!compare(ptr2[i], wordcount, a, b)) {
      free(ptr);
      free(ptr2);
      return 0;
    }
  free(ptr);
  free(ptr2);
  return 1;
}

#ifndef __PROGTEST__
int main(int argc, char *argv[]) {
  assert ( sameWords ( "Hello students.", "HELLO studEnts!" ) == 1 );
  assert ( sameWords ( " He said 'hello!'", "'Hello.' he   said." ) == 1 );
  assert ( sameWords ( "He said he would do it.", "IT said: 'He would do it.'" ) == 1 );
  assert ( sameWords ( "one two three", "one two five" ) == 0 );

  return 0;
}
#endif /* __PROGTEST__ */
