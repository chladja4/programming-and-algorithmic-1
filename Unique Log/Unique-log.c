#include <stdio.h>
#define ARRAY_SIZE 1000001
int main () {
    static int REMEMBER_ORIGIN[ARRAY_SIZE]={0};
    int ID, from, to,Position=0,ORIGINS,res;
    static int UNIQ[100000]={0};
    static int NUM_SER[ARRAY_SIZE]={0};
    printf("Pozadavky:\n");
    //scan input until EOF or wrong input detected
    while (1) {
        // if ID scanned
        if ((res=scanf(" +%d", &ID)) == 1&&ID>=0&&ID<100000) {
            //place ID into array move pointer to next position
            NUM_SER[Position]=ID;
            Position++;
            if(Position>1000000){
               printf("Nespravny vstup.\n");
                return 0;
            }
            //check if it is first,second... login
            if(UNIQ[ID]==0){
                printf ("> prvni navsteva\n");
                UNIQ[ID]+=1;
            }
            else{
                UNIQ[ID]+=1;
                printf("> navsteva #%d\n",UNIQ[ID]);
            }
        }
        // if request scanned
        else if ((res=scanf("? %d%d", &from, &to)) == 2&&from>=0&&to>=0&&from<=to&&to<=Position-1){
            ORIGINS=0;
            //save from to tmp bcs of changes in for loop
            int tmp=from;
            for(;from<=to;from++){
                //checks position of id, updates if new
                if(REMEMBER_ORIGIN[NUM_SER[from]]==0){
                        REMEMBER_ORIGIN[NUM_SER[from]]=1;
                        ORIGINS++;}
             }
            from=tmp;
            //cleaning array
            for(;from<=to;from++){
                REMEMBER_ORIGIN[NUM_SER[from]]=0;
            }
                printf("> %d / %d\n",ORIGINS,to-tmp+1);
        }
        else break;
        }
    //wrong input check
    if(res!=EOF){
        printf("Nespravny vstup.\n");
        return 0;
    }
    else return 0;
}
